-- SUMMARY --

This is basically an extension module for feeds and feeds tamper. To my
understanding, it works in the exact opposite way of
Synonyms(https://www.drupal.org/project/synonyms). Instead of accepting and
incorporating synonyms, it tries to exterminate them.

The main reason I created this module and propably its better use case is for
importing data through feeds and feeds_tamper.

-- REQUIREMENTS --

feeds https://www.drupal.org/project/feeds
feeds_tamper https://www.drupal.org/project/feeds_tamper
taxonomy

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- Setup --

(It's coming)
